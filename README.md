## Drupal Next.Js Recipe

This recipe is designed to do the following:

- Add Next.Js support in Drupal
- Add Layout Builder exposure to JSON:API output

## Installing

- Start with a Drupal 10.3+ site.
- Apply the recipe

Execute this command from the web root:

```shell
php core/scripts/drupal recipe recipes/drupal-nextjs-recipe
```

Or by using `ddev exec`

```shell
ddev exec -d /var/www/html/[web-root] php core/scripts/drupal recipe recipes/drupal-nextjs-recipe
```

If all goes well, you should see the following output:

```shell
 [OK] Drupal Next.Js applied successfully
```

Clear the cache after the recipe is applied. When going back to the site,
all the recipe configuration and customization has been applied.

This recipe also requires the following patches in `composer.json`:

```json
	"patches": {
	  "drupal/core": {
		"Harden the use of unserialize in InlineBlock via allowed classes": "https://git.drupalcode.org/project/drupal/-/merge_requests/4926.diff",
		"[PP-1] Expose Layout Builder data to REST and JSON:API": "https://www.drupal.org/files/issues/2024-01-18/2942975-255.patch",
		"Layout builder does not protect against recursion": "https://www.drupal.org/files/issues/2023-04-27/2979184-90.patch",
		"Layout builder does not protect against recursion (trigger warning)": "https://www.drupal.org/files/issues/2023-04-27/diff-90-87.patch",
		"ModuleHandler skips all hook implementations when invoked before the module files have been loaded": "https://www.drupal.org/files/issues/2023-03-10/3207813-27.patch"
	  },
	  "drupal/jsonapi_include_lb": {
		"Rework module to work without a separate computed field": "patches/local/jsonapi_include_lb/3374355-9.patch"
	  },
	  "drupal/pathauto": {
		"join-path token adds <nolink> menu items to url path and path generated is created from menu titles, not parent path links": "https://www.drupal.org/files/issues/2023-01-06/remove-no-links-3214658-27.patch"
	  },
	  "drupal/subrequests": {
		"Get same results on different request": "https://www.drupal.org/files/issues/2019-07-18/change_request_type-63049395-09.patch"
	  },
	  "drupal/decoupled_router": {
		"Unable to resolve path on node in other language than default": "https://www.drupal.org/files/issues/2022-11-30/decouple_router-3111456-resolve-language-issue-58.patch"
	  }
	}
```